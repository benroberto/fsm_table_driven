/**
* @file  fsm_fault_standby.c
* @brief Ariens Fault FSM table driven concept study.
*
* @brief This file contains the functionality of the Ariens Fault FSM STANDBY state.
*
* REVISION HISTORY
*==============================================
* |Ticket   |Date      |Author       |Notes
* |----:    |:----:    |:----:       |:----
* |Creation |02/21/22  |broberto     |Document
*
* @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
*/

#include <stdio.h>

#include "fsm_fault_globals.h"
#include "fsm_fault.h"

#include "fsm_fault_standby.h"

/**
* @brief Returns an event code based on the FSM_var data.
*
* @param[in] struct pointer to FSM_var
*
* @return event code
*
* @brief from spreadsheet:
* @brief gb_ship = 1 && gb_ignition = 0
* @brief goto SHIP state
*
* @brief gb_intlk = 1 && gb_ignition = 1
* @brief goto DSG state
*/
enum event_code state_standby(const struct FSM_var* p_myFSM_var)
{
    fprintf(stdout, "STATE: %s\n", __func__);

    if (1 == p_myFSM_var->b_ship &&
        0 == p_myFSM_var->b_ignition) {
        return EC_SHIP_IGN;
    }
    else if (1 == p_myFSM_var->b_intlk &&
             1 == p_myFSM_var->b_ignition) {
        return EC_INTLK_IGN;
    }
#if 0
    else {
        /**
         * we really need something here
         * maybe mitigate a potential hazard situation
         */
        return EC_FAIL;
    }
#endif
}

