/**
* @file  fsm_fault_fault.H
* @brief Ariens Fault FSM table driven concept study.
*
* @brief Header file of the Ariens Fault FSM _FAULT_ state.
*
* REVISION HISTORY
*==============================================
* |Ticket   |Date      |Author       |Notes
* |----:    |:----:    |:----:       |:----
* |Creation |02/21/22  |broberto     |Document
*
* @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
*/

#if !defined(_FSM_FAULT_FAULT_H_)
#define _FSM_FAULT_FAULT_H_

enum event_code state_fault(const struct FSM_var*);

#endif /* _FSM_FAULT_FAULT_H_ */