/**
* @file  fsm_fault.H
* @brief Ariens Fault FSM table driven concept study.
*
* @brief Header file for the Ariens Fault FSM global variables derived
* from FILE main_fsm_table_driven.c.
*
* REVISION HISTORY
*==============================================
* |Ticket   |Date      |Author       |Notes
* |----:    |:----:    |:----:       |:----
* |Creation |02/21/22  |broberto     |Document
*
* @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
*/

#if !defined(_FSM_FAULT_GLOBALS_H_)
#define _FSM_FAULT_GLOBALS_H_

#include <stdbool.h>

/** struct to hold 'global' values from the test file  */
struct FSM_var {
    enum state_code curr_state; /**< initial starting state */
    bool b_cell_voltages;       /**< cell voltage flag */
    bool b_chg_safety;          /**< charge comms flag */
    bool b_chgr_comms;          /**< charge comms flag */
    bool b_dsg_safety;          /**< discharge safety  */
    bool b_ignition;            /**< ignition flag */
    bool b_intlk;               /**< interlock flag */
    bool b_ship;                /**< ship flag */
    bool b_soc_button;          /**< SoC pushbutton flag */
};

extern bool b_intlk_ALT;
extern bool b_soc_button_ALT;

#endif

