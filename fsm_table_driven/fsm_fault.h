/**
 * @file  fsm_fault.H
 * @brief Ariens Fault FSM table driven concept study.
 *
 * Description:
 * Header file for the Ariens Fault FSM.
 *
 * REVISION HISTORY
 *==============================================
 * |Ticket   |Date      |Author       |Notes
 * |----:    |:----:    |:----:       |:----
 * |Creation |02/21/22  |broberto     |Document
 *
 * @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
 */

#if !defined(_FSM_FAULT_H_)
#define _FSM_FAULT_H_

/**
 * event codes can be in any order
 */
enum event_code {
    EC_SHIP,
    EC_SHIP_IGN,
    EC_INTLK_IGN,
    EC_CHG_COMM_CELL,
    EC_INTLK_SOC,
    EC_COMM_LOST,
    EC_DSG_OK,
    EC_CHG_OK,
    EC_FAIL,
    MAX_RETURN_CODE
};

/**
 * state codes
 * Function pointer array and enum state codes must be in sync
 * Function ptr array in FILE: _fsm_fault.c_
 */
enum state_code {
    SC_SHIP,
    SC_STANDBY,
    SC_DISCHARGE,
    SC_CHARGE,
    SC_FAULT,
    MAX_STATE_CODE
};

enum state_code FSM_Fault_curr_state;

enum state_code FSM_Fault_Run(const struct FSM_var* p_myFSM_var, enum state_code curr_state); /* FSM starts here */

#endif /* _FSM_FAULT_H_ */

