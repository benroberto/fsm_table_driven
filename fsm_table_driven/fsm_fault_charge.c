/**
* @file  fsm_fault_charge.c
* @brief Ariens Fault FSM table driven concept study.
*
* This file contains the functionality of the Ariens Fault FSM CHARGE state.
*
* REVISION HISTORY
*==============================================
* |Ticket   |Date      |Author       |Notes
* |----:    |:----:    |:----:       |:----
* |Creation |02/21/22  |broberto     |Document
*
* @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
*/

#include <stdio.h>

#include "fsm_fault_globals.h"
#include "fsm_fault.h"

#include "fsm_fault_charge.h"

/**
* @brief Returns an event code based on the FSM_var data.
*
* @param[in] struct pointer to FSM_var
*
* @return event code
*
* @brief from spreadsheet:
* @brief gb_chg_safety = 1
* @brief goto FAULT state
*
* @brief gb_chgr_comms = 0
* @brief goto DSG state
*
* @brief gb_intlk = 0 || gb_ignition = 0
* @brief goto STANDBY state
*/
enum event_code state_charge(const struct FSM_var *p_myFSM_var)
{
    fprintf(stdout, "STATE: %s\n", __func__);

    if (1 == p_myFSM_var->b_chg_safety) {
        return EC_FAIL;
    }
    else if (0 == p_myFSM_var->b_chgr_comms) {
        return EC_COMM_LOST;
    }
    else if ( (0 == p_myFSM_var->b_intlk ||
               0 == p_myFSM_var->b_ignition) &&
               1 == p_myFSM_var->b_chgr_comms ) {
        return EC_INTLK_IGN;
    }

#if 0
    else {
        /**
         * we really need something here
         * maybe mitigate a potential hazard situation
         */
        return EC_FAIL;
    }
#endif
}
