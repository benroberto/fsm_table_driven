/**
* @file  fsm_fault_ship.c
* @brief Ariens Fault FSM table driven concept study.
*
* @brief This file contains the functionality of the Ariens Fault FSM SHIP state.
*
* REVISION HISTORY
*==============================================
* |Ticket   |Date      |Author       |Notes
* |----:    |:----:    |:----:       |:----
* |Creation |02/21/22  |broberto     |Document
*
* @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
*/

#include <stdio.h>

#include "fsm_fault_globals.h"
#include "fsm_fault.h"

#include "fsm_fault_ship.h"

/**
* @brief Returns an event code based on the FSM_var data.
*
* @param[in] struct pointer to FSM_var (not used for this example)
*
* @return event code
*
* @brief This function uses an alternative to the FSM_var struct pointer.
* @brief The global vars are accessed by externs called out in FILE: fsm_fault_globals.h.
*
* @brief from spreadsheet:
* @brief gb_intlk = 1 || gb_soc_button = 1
* @brief STANDBY state
*/
enum event_code state_ship(const struct FSM_var *p_myFSM_var)
{
    fprintf(stdout, "STATE: %s\n", __func__);

#if 0
    if (1 == p_myFSM_var->b_intlk ||
        1 == p_myFSM_var->b_soc_button) {
        return EC_INTLK_SOC;
    }
    else {
        /**
         * we really need something here
         * maybe mitigate a potential hazard situation
         */
        return EC_SHIP; /* stay here until valid flags */
    }

#else
    /* alternative to FSM_var struct/pointer */
    if (1 == b_intlk_ALT ||
        1 == b_soc_button_ALT) {
        return EC_INTLK_SOC;
    }
    else {
        /**
         * we really need something here
         * maybe mitigate a potential hazard situation
         */
        return EC_SHIP; /* stay here until valid flags */
    }
#endif
}

