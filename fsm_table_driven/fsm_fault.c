/**
* @file  fsm_fault.c
* Ariens Fault FSM table driven concept study.
*
* - This file contains the functionality of the Ariens Fault FSM.
* - This is only a concept study for table driven FSMs.
*
* REVISION HISTORY
*==============================================
* |Ticket   |Date      |Author       |Notes
* |----:    |:----:    |:----:       |:----
* |Creation |02/21/22  |broberto     |Document
*
* @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
*/

#include <stdio.h>
#include <stdint.h>

#include "fsm_fault_ship.h"
#include "fsm_fault_standby.h"
#include "fsm_fault_discharge.h"
#include "fsm_fault_charge.h"
#include "fsm_fault_fault.h"

#include "fsm_fault_globals.h"
#include "fsm_fault.h"

/**
* Array of function ptrs for state functions
* - Function pointer array and enum STATE CODES must be in sync.
* STATE CODES in FILE: fsm_fault.h
*/
enum event_code (*const FSM_Fault_state[])(const struct FSM_var* p_myFSM_var) = {
    state_ship,
    state_standby,
    state_discharge,
    state_charge,
    state_fault,
};

/**
* @brief struct for state transition table
* @brief <current state>, <event code>, <next state>
*/
struct transition {
    enum state_code current_state;
    enum event_code event;
    enum state_code next_state;
};

/**
* array of structs for state transition table.
* - <current state>, <event code>, <next state>
*/
struct transition state_transition_tbl[] = {
    {SC_SHIP,       EC_SHIP,           SC_SHIP},
    {SC_SHIP,       EC_INTLK_SOC,      SC_STANDBY},
    {SC_STANDBY,    EC_SHIP_IGN,       SC_SHIP},
    {SC_STANDBY,    EC_INTLK_IGN,      SC_DISCHARGE},
    {SC_DISCHARGE,  EC_FAIL,           SC_FAULT},
    {SC_DISCHARGE,  EC_INTLK_IGN,      SC_STANDBY},
    {SC_DISCHARGE,  EC_CHG_COMM_CELL,  SC_CHARGE},
    {SC_CHARGE,     EC_FAIL,           SC_FAULT},
    {SC_CHARGE,     EC_COMM_LOST,      SC_DISCHARGE},
    {SC_CHARGE,     EC_INTLK_IGN,      SC_STANDBY},
    {SC_FAULT,      EC_SHIP,           SC_SHIP},
    {SC_FAULT,      EC_INTLK_IGN,      SC_STANDBY},
    {SC_FAULT,      EC_DSG_OK,         SC_DISCHARGE},
    {SC_FAULT,      EC_CHG_OK,         SC_CHARGE},
};
/**
* @brief max number of state transition entries.
*/
#define MAX_STATE_TRANSITIONS (sizeof state_transition_tbl / sizeof state_transition_tbl[0])

#define EXIT_STATE_CODE SC_FAULT

enum state_code find_next_state(enum state_code, enum event_code);

/**
* Table-driven FSM derived from Ariens Fault FSM
*
* @param struct_pointer to a copy of global vars FSM_var.
* @param current_state
*
* @return next_state is found in the transition table
*
* - The FSM reads the global data from a struct including the initial(current) STATE CODE.
* - S1. Based on the STATE CODE, the FSM use the lookup table for the initial state function.
* - S2. The state function will return an EVENT CODE based on the global data.
* - S3. The next state is found in the transition table based on the current STATE CODE and EVENT CODE
* in fn find_next_state()
* - When the FSM runs again, Steps 1 through 3 will be repeated.
*/
enum state_code FSM_Fault_Run(const struct FSM_var* p_myFSM_var, enum state_code curr_state)
{
    enum state_code next_state;
    enum event_code event;
    enum event_code(*state_fn)(const struct FSM_var*);

    state_fn = FSM_Fault_state[curr_state];
    event = state_fn(p_myFSM_var);
    next_state = find_next_state(curr_state, event);

    return (next_state);
}

/**
* @brief Finds the next state in the TRANSITION table.
*
* @param current_state
* @param event_code from the previous state function call.
*
* @return next_state is found in the transition table based on the current STATE CODE and EVENT CODE.
*/
enum state_code find_next_state(enum state_code curr_state, enum event_code event)
{
    //fprintf(STDOUT, "%s\n", __func__);

    enum state_code next_state;
    bool next_state_found = 0;

    for (int i = 0; i < MAX_STATE_TRANSITIONS; i++) {
        if (curr_state == state_transition_tbl[i].current_state &&
            event == state_transition_tbl[i].event) {
            next_state = state_transition_tbl[i].next_state;
            next_state_found = 1;
            break;
        }
    }

    if (0 == next_state_found) {
        next_state = EXIT_STATE_CODE; /* for safety (maybe) */
    }
    return (next_state);
}
