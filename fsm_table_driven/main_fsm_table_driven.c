/**
* @file  main_fsm_table_driven.c
* @brief This file is contains the command line wrapper for the
* Behavorial Test of the Ariens Fault FSM.
*
* @brief This is only a concept study for table driven FSMs.
*
* @brief This project was built in Visual Studio 2019 as a
* console app (command line exe).
*
* REVISION HISTORY
*==============================================
* |Ticket   |Date      |Author       |Notes
* |----:    |:----:    |:----:       |:----
* |Creation |02/21/22  |broberto     |Document
*
* @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
*/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include "fsm_fault_globals.h"
#include "fsm_fault.h"

char g_var_name[9][20]; /**< test file string name of global vars */
int  g_var_val[9];      /**< test file values of global vars      */

void SYS_fsm_load_globals(struct FSM_var*);

/**
* @brief Alternative to FSM struct pointer access
* @brief Declare/initialize FSM_Fault module scope vars here.
* @brief To share among the world, use externs for the following in a SINGLE header file.
* @brief Look in FILE: fsm_fault_globals.h and FILE: fsm_fault_ship.c for implementation.
*/
bool b_intlk_ALT;      /**< interlock flag alternative to FSM_var struct/pointer */
bool b_soc_button_ALT; /**< SoC button flag alternative to FSM_var struct/pointer*/

/**
* Wrapper for Behavorial Test of Ariens Fault FSM.
*
* @param test_file containing initial global values.
*
* @return  0
* @return  1 on error
*
* - The values are temporarily stored in arrays.
* - After the array values are stored in a struct, the struct
* is read by the FSM.
* - The FSM runs 3 times and exits.
*/
int main(int argc, char* argv[])
{
    fprintf(stdout, "%s(): Fault FSM RUNNING\n", __func__);

    FILE *in_file; /* test file */

    struct FSM_var myFSM_var;    /*! FSM interface storage to hold global vars */
    struct FSM_var* p_myFSM_var; /**< FSM interface struct pointer for access */
    p_myFSM_var = &myFSM_var;

    enum state_code next_state;

    /* check test file */
    if (argc != 2) {
		fprintf(stdout, "Note enough args.\n");
        exit(1);
	}
    else if (fopen_s(&in_file, argv[1], "r") != 0) {
        fprintf(stdout, "Error opening file.\n");
        exit(1);
    }

    /* read the test file values and store */
    int i = 0; /**< doxygen test */
    while (!feof(in_file)) {
        fscanf_s(in_file, "%s", &g_var_name[i], sizeof g_var_name); /**< sizeof required for strings */
        fscanf_s(in_file, "%d", &g_var_val[i]);                     /**< sizeof not required for type */
        //fprintf(stdout, "g_var_name: %s    g_var_val: %d\n", g_var_name[i], g_var_val[i]); /* sanity check */
        i++;
    }

    fclose(in_file); /* done with the test file */

    SYS_fsm_load_globals(p_myFSM_var); /**< load test file values into FSM struct for read-only operations */

    next_state = p_myFSM_var->curr_state;

    for (i = 0; i < 3; i++) {
        /*-------------------------------------------------------*/
        /*---- function under test ------------------------------*/

        next_state = FSM_Fault_Run(p_myFSM_var, next_state);

        /*-------------------------------------------------------*/
    }
    /**
    * NOTE: var next_state should never be changed outside of the FSM.
    * Doing so can break the system (and your fingers).
    */

    fprintf(stdout, "%s(): Fault FSM test complete\n", __func__);

    return (0);
}

/**
* @brief Load 'global' values from arrays into a struct
* to be read by the FSM.
*
* This is an example of a SYSTEM level interface to another module.
*
* @param struct_pointer to a copy of global data FSM_var
*
* @return none
*
* - fn SYS_fsm_load_globals() is always called before calling the FSM
* to insure having the latest data.
* - The global vars are read from a test file and stored in arrays.
* - The array data is copied into the FSM struct for encapsulation.
*
* @brief Under real world conditions the global variables are visible
* -- ex. g_cell_voltages -- and loading the FSM interface would be:
* \code{.c}
*      p_myFSM_var->b_cell_voltages = g_cell_voltages;
* \endcode
*
* @brief An alternative to using a structure is to use global vars
* above main and declare externs in FILE fsm_fault_globals.h
*/
void SYS_fsm_load_globals(struct FSM_var* p_myFSM_var)
{
    /* FSM global variables */
    p_myFSM_var->curr_state      = g_var_val[0];
    p_myFSM_var->b_cell_voltages = g_var_val[1];
    p_myFSM_var->b_chg_safety    = g_var_val[2];
    p_myFSM_var->b_chgr_comms    = g_var_val[3];
    p_myFSM_var->b_dsg_safety    = g_var_val[4];
    p_myFSM_var->b_ignition      = g_var_val[5];
    p_myFSM_var->b_intlk         = g_var_val[6];
    p_myFSM_var->b_ship          = g_var_val[7];
    p_myFSM_var->b_soc_button    = g_var_val[8];

    /* alternative to FSM_var struct/pointer */
    b_intlk_ALT      = g_var_val[6];
    b_soc_button_ALT = g_var_val[8];

#if 0
    /* sanity check */
    fprintf(stdout, "g_var_name: %s    g_var_val: %d\n", g_var_name[0], p_myFSM_var->curr_state);
    fprintf(stdout, "g_var_name: %s    g_var_val: %d\n", g_var_name[1], p_myFSM_var->b_cell_voltages);
    fprintf(stdout, "g_var_name: %s    g_var_val: %d\n", g_var_name[2], p_myFSM_var->b_chg_safety);
    fprintf(stdout, "g_var_name: %s    g_var_val: %d\n", g_var_name[3], p_myFSM_var->b_chgr_comms);
    fprintf(stdout, "g_var_name: %s    g_var_val: %d\n", g_var_name[4], p_myFSM_var->b_dsg_safety);
    fprintf(stdout, "g_var_name: %s    g_var_val: %d\n", g_var_name[5], p_myFSM_var->b_ignition);
    fprintf(stdout, "g_var_name: %s    g_var_val: %d\n", g_var_name[6], p_myFSM_var->b_intlk);
    fprintf(stdout, "g_var_name: %s    g_var_val: %d\n", g_var_name[7], p_myFSM_var->b_ship);
    fprintf(stdout, "g_var_name: %s    g_var_val: %d\n", g_var_name[8], p_myFSM_var->b_soc_button);

    /* alternative to FSM_var struct/pointer*/
    fprintf(stdout, "g_var_name: %s    b_intlk_ALT: %d\n",      g_var_name[6], b_intlk_ALT);
    fprintf(stdout, "g_var_name: %s    b_soc_button_ALT: %d\n", g_var_name[8], b_soc_button_ALT);
#endif
}
