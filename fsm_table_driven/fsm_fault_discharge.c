/**
* @file  fsm_fault_discharge.c
* @brief Ariens Fault FSM table driven concept study.
*
* @brief This file contains the functionality of the Ariens Fault FSM DISCHARGE state.
*
* REVISION HISTORY
*==============================================
* |Ticket   |Date      |Author       |Notes
* |----:    |:----:    |:----:       |:----
* |Creation |02/21/22  |broberto     |Document
*
* @par   COPYRIGHT NOTICE: (c) 2022 Inventus Power.  All rights reserved.
*/

#include <stdio.h>

#include "fsm_fault_globals.h"
#include "fsm_fault.h"

#include "fsm_fault_discharge.h"

/**
* @brief Returns an event code based on the FSM_var data.
*
* @param[in] struct pointer to FSM_var
*
* @return event code
*
* @brief from spreadsheet:
* @brief gb_dsg_safety = 1
* @brief goto FAULT state
*
* @brief gb_intlk = 0 || gb_ignition = 0
* @brief goto STANDBY state
*
* @brief gb_chgr_comms = 1 && gb_all_cell_voltage = 1
* @brief goto CHARGE STATE
*/
enum event_code state_discharge(const struct FSM_var *p_myFSM_var)
{
    fprintf(stdout, "STATE: %s\n", __func__);

    /* ex call for cell voltages
    *       b_cell_voltages = Sys_check_cell_volts_3V();
    */
#if 1
/* correct operation */
    if (1 == p_myFSM_var->b_dsg_safety) {
        return EC_FAIL;
    }
    else if (1 == p_myFSM_var->b_chgr_comms &&
        1 == p_myFSM_var->b_cell_voltages) {

        /* do stuff before leaving */
        return EC_CHG_COMM_CELL;
    }
    else if (0 == p_myFSM_var->b_intlk ||
             0 == p_myFSM_var->b_ignition) {
        return EC_INTLK_IGN;
    }
#else
    /* different result */
    if (1 == p_myFSM_var->b_dsg_safety) {
        return EC_FAIL;
    }
    else if (0 == p_myFSM_var->b_intlk ||
        0 == p_myFSM_var->b_ignition) {
        return EC_INTLK_IGN;
    }
    else if (1 == p_myFSM_var->b_chgr_comms &&
        1 == p_myFSM_var->b_cell_voltages) {
        return EC_CHG_COMM_CELL;
    }
#endif

#if 0
    else {
        /**
         * we really need something here
         * maybe mitigate a potential hazard situation
         */
        return EC_SHIP;
    }
#endif
}
