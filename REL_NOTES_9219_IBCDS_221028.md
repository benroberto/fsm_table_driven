# 22.10.28
# Release Notes for RC_9219_IBCDS_v221028
### Files for this Release Candidate:
- RC_9219_IBCDS_C1_v221028.hex
    - Intel HEX file for the User Interface (UI) PCBA.
- 9219_IBCDS_EEPROM_CFG_v221028.hex
    - Intel HEX file to reset the CONFIGURATION BLOCK of the (UI) PCBA.
- This file.
# SETUP
- Two files will be programmed in order onto the User Interface PCBA:
1. 9219_IBCDS_EEPROM_CFG_v221028.hex
2. RC_9219_IBCDS_C1_v221028.hex
# OPERATIONAL
- 2 minute warm up
# USER OPTIONS
- Press the ANYKEY -> Debug out
- Press Audible Alarm Pushbutton -> EEPROM dump
# OPS
- Limits
	- CO2: 1000ppm
	- H2: 16000ppm
# KNOWN BUGS
- Audible Alarm will be solid ON while dumping EEPROM.
- If all 3 boards are system faulted AND battery evented, alarm may chirp randomly.
# Programming the User Interface PCBA
1. Connect the User Interface (UI) Rack Device power cord to 120VAC.
2. Connect a Microchip ICD4 or SNAP programmer to the programming header.
3. Start MPLAB IPE v6.x.
4. In MPLAB IPE > Operate Tab > FAMILY
    - Select ALL FAMILIES
5. In MPLAB IPE > Operate Tab > DEVICE
    - Select PIC24FJ256GA702
6. In MPLAB IPE > Operate Tab > TOOL
    - Select either ICD4 or SNAP option
7 In MPLAB IPE > Select Options > Power
    - If ICD4 will be used to program the device, Deselect POWER TARGET CIRCUIT FROM ICD4.
    - No action required for SNAP.
8. In MPLAB IPE > Select Options > Settings
    - Set SEGMENTS TO BE PROGRAMMED to FULL CHIP PROGRAMMING.
9. In MPLAB IPE > Select Options > Settings
    - Select PROGRAM SPEED to LOW.
10. In MPLAB IPE > Select Operate > Tool
    - Select desired programming tool.
11. In MPLAB IPE > Select Operate > Hex File
    - Select programming file.
12. Turn ON User Interface Rack device.
13. In MPLAB IPE > Select Operate > Connect
    - MPLAB IPE should connect to the UI without errors.
14. In MPLAB IPE > Select Operate > Program EEPROM config build
    - MPLAB IPE should program the UI without errors.
    - Should see terminal output shown in sample output section
15. In MPLAB IPE > Select Operate > Disconnect
16. In MPLAB IPE > Select Operate > Connect
    - MPLAB IPE should connect to the UI without errors.
17. In MPLAB IPE > Select Operate > Program release candidate build
    - MPLAB IPE should program the UI without errors.
    - Terminal output shows system started 2 minute warmup.
    - SYSTEM OK Green LED: ON/OFF at 1/2 Hz.
# SAMPLE OUTPUT
- EEPROM CFG BUILD
	- Sample output: (no records stored):

	 Interim BCDS Eeprom Config  Oct 28 2022  17:21:07

	 Calculated record count: 0
	 Configuration block initialized.

	- Sample output: (x records stored):

	 Interim BCDS Eeprom Config  Oct 28 2022  17:21:07

	 Calculated record count: 10
	 Are you sure you want to delete existing data?(pb = yes)
	 Configuration block initialized.